load('ALL_FANS_V2.mat');

fan_names = {'GC', 'HP', 'SO1', 'SO2'};
fans = {gc_data, hp_data, so1_data, so2_data};

for j=1:length(fans)

    
        fan = fans{j};
        fan_surface_names = {};
        fan_name = fan_names{j};
        
        % For each surface
       for si=1:length(fan)

           cs = fan(si);
           cs = cs{1};
           fan_surface_name = [fan_name '_' cs.name];

           wolmans_all = [];
           
           for w = 1:length(cs.wolmans)
               wolmans_all = [wolmans_all; cs.wolmans{w}];
           end
           
           writematrix(wolmans_all,['full_surface_GSD_no_weights/' ...
               fan_surface_name '.csv']);
           
       end
end
