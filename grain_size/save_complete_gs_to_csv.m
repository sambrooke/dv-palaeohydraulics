% A script to convert the .mat grain size data for fans into csv format
% 
% This will export all grain size measurements per fan surface to a single
% .csv file

addpath('lib');

load('ALL_FANS_V2.mat');

fan_names = {'GC', 'HP', 'SO1', 'SO2', 'UB', 'G10', 'G8', 'T1'};
fans = {gc_data, hp_data, so1_data, so2_data, ub_data, g10_data, g8_data, t1_data};

% [s_figs, s_stats] = surface_plots(fan_names{2}, fans{2});
 
[distance_sorted] = surface_stats(fan_names, fans); % Writes to full_surface_GSD_no_sites

HP_Q4 = distance_sorted.HP.Surface_A;
HP_Q3 = distance_sorted.HP.Surface_B;
HP_Q2 = distance_sorted.HP.Surface_C;

GC_Q4 = distance_sorted.GC.Surface_A;
GC_Q3 = distance_sorted.GC.Surface_B;
GC_Q2 = distance_sorted.GC.Surface_D;

SO1_Q4 = distance_sorted.SO1.Surface_4;
SO1_Q3 = distance_sorted.SO1.Surface_3b;
SO1_Q2 = distance_sorted.SO1.Surface_2a;

SO2_Q4 = distance_sorted.SO2.Surface_4;
SO2_Q3 = distance_sorted.SO2.Surface_3b;
SO2_Q2 = distance_sorted.SO2.Surface_2;

SO2_Q4 = distance_sorted.SO2.Surface_4;
SO2_Q3 = distance_sorted.SO2.Surface_3b;
SO2_Q2 = distance_sorted.SO2.Surface_2;

export_to_file(HP_Q4, 'distance_sorted/HP_Q4.csv')
export_to_file(HP_Q3, 'distance_sorted/HP_Q3.csv')
export_to_file(HP_Q2, 'distance_sorted/HP_Q2.csv')

export_to_file(GC_Q4, 'distance_sorted/GC_Q4.csv')
export_to_file(GC_Q3, 'distance_sorted/GC_Q3.csv')
export_to_file(GC_Q2, 'distance_sorted/GC_Q2.csv')

export_to_file(SO1_Q4, 'distance_sorted/SO1_Q4.csv')
export_to_file(SO1_Q3, 'distance_sorted/SO1_Q3.csv')
export_to_file(SO1_Q2, 'distance_sorted/SO1_Q2.csv')

export_to_file(SO2_Q4, 'distance_sorted/SO2_Q4.csv')
export_to_file(SO2_Q3, 'distance_sorted/SO2_Q3.csv')
export_to_file(SO2_Q2, 'distance_sorted/SO2_Q2.csv')

export_to_file(distance_sorted.UB.Surface_A, 'distance_sorted/UB_A.csv')
export_to_file(distance_sorted.UB.Surface_C, 'distance_sorted/UB_C.csv')
export_to_file(distance_sorted.UB.Surface_D, 'distance_sorted/UB_D.csv')

export_to_file(distance_sorted.G10.Surface_A, 'distance_sorted/G10_A.csv')
export_to_file(distance_sorted.G10.Surface_B, 'distance_sorted/G10_B.csv')
export_to_file(distance_sorted.G10.Surface_C, 'distance_sorted/G10_C.csv')
export_to_file(distance_sorted.G10.Surface_D, 'distance_sorted/G10_D.csv')
export_to_file(distance_sorted.G10.Surface_E, 'distance_sorted/G10_E.csv')

export_to_file(distance_sorted.G8.Surface_A, 'distance_sorted/G8_A.csv')
export_to_file(distance_sorted.G8.Surface_B, 'distance_sorted/G8_B.csv')
export_to_file(distance_sorted.G8.Surface_C, 'distance_sorted/G8_C.csv')
export_to_file(distance_sorted.G8.Surface_D, 'distance_sorted/G8_D.csv')

export_to_file(distance_sorted.T1.Surface_A, 'distance_sorted/T1_A.csv')
export_to_file(distance_sorted.T1.Surface_C, 'distance_sorted/T1_C.csv')
export_to_file(distance_sorted.T1.Surface_E, 'distance_sorted/T1_E.csv')

