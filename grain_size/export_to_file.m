
function export_to_file(distance_sorted_cell, output_file)
    
    dims = size(distance_sorted_cell);

    distance_m = cell2mat(distance_sorted_cell(:,1));
    D25_mm = zeros(dims(1),1);
    D50_mm = zeros(dims(1),1);
    D75_mm = zeros(dims(1),1);
    D84_mm = zeros(dims(1),1);
    D90_mm = zeros(dims(1),1);
    means_mm = zeros(dims(1),1);
    stdevs_mm = zeros(dims(1),1);
    latitude_utm = zeros(dims(1),1);
    longitude_utm = zeros(dims(1),1);
    site_id = cell(dims(1),1);
    
    for i = 1:dims(1)
       wolman_weighted = distance_sorted_cell{i,2};
       wolman_weighted_no_nan = wolman_weighted(~isnan(wolman_weighted));
       D25_mm(i) = prctile(wolman_weighted_no_nan,25);
       D50_mm(i) = prctile(wolman_weighted_no_nan,50);
       D75_mm(i) = prctile(wolman_weighted_no_nan,75);
       D84_mm(i) = prctile(wolman_weighted_no_nan,84);
       D90_mm(i) = prctile(wolman_weighted_no_nan,90);
       means_mm(i) = mean(wolman_weighted_no_nan);
       stdevs_mm(i) = std(wolman_weighted_no_nan);
       latitude_utm(i) = distance_sorted_cell{i,3}(2);
       longitude_utm(i) = distance_sorted_cell{i,3}(1);
       site_id{i} = distance_sorted_cell{i,4};
    end

    T = table(site_id,latitude_utm, longitude_utm, distance_m, means_mm, stdevs_mm, ...
        D25_mm, D50_mm, D75_mm, D84_mm, D90_mm);

    writetable(T, output_file);
    
end
