% This function processes site-by-site grain size measurements to ensure
% the correct weighting observed between multiple Wolman point counts at 
% a given location. This often happens when there's clear fractionation
% i.e. bar and swale morphology on alluvial fan surfaces

function [distance_sorted] = surface_stats(fan_names, fans)

    distance_sorted = struct();
    figures = struct();
    
    for j=1:length(fans)
        fan = fans{j};
        fan_surface_names = {};
        fan_name = fan_names{j};
        groups = {};

        % For each surface
       for si=1:length(fan)

        % Current surface
        cs = fan(si);
        cs = cs{1};
        s_group = cell(1,2);

        site_wolmans = struct();

        fan_surface_names = [fan_surface_names, ['Surface_' cs.name]];

        % Loop over sites
        for ri=1:length(cs.meta)

            cs_meta = cs.meta{ri};
            cover = cs_meta.cover;
            c_name = cs_meta.c_name;
            cda.name = cs_meta.name;
            cda.cover = cover;
            cda.coords = cs.coords(ri,:);
            cda.id = cs_meta.w_id;
            cda.distance = cs.distance(cda.id);
            cda.wolman = cs.wolmans{cda.id};
            sp = strsplit(cs_meta.name, '_');
            samp_prefix = sp{1};
            t = isstrprop(samp_prefix, 'digit');
            gname = '';

            for tt=1:length(t)
               if t(tt) > 0
                gname = [gname, num2words_fast(str2num(samp_prefix(tt)))];
               else
                gname = [gname, samp_prefix(tt)];
               end
            end

            if strcmp(gname,'')
                gname = 'nil';
            end
            
            gname = strrep(gname, ' ', '_');

            % Check if site name exists (eg. t1E-9)
            if sum(strcmp(s_group(:,1), c_name)) > 0
                idx = find(strcmp(c_name,s_group(:,1)));

                % Does sample name prefix exist?
                current_fieldnames = fieldnames(s_group{idx,2});
                rank = [];

                for f=1:length(current_fieldnames)
                    % Comparing
                    rank = [rank, strdist(gname, current_fieldnames{f})];
                end

                [M,I] = min(rank);
                if M > 2
                    s_group{idx,2}.(gname) = cda;
                else
                    fname = current_fieldnames{I};
                    s_group{idx,2}.(fname) = [s_group{idx,2}.(fname), cda];         
                end

            else
                strcat(gname, c_name);
                s_group = [s_group;{c_name, struct(gname, cda)}];
            end
            
            if max(cda.wolman) > 1000
               disp([cs.name ' '  c_name])
            end
        end
        
        s_group(1,:) = [];
        groups{si} = s_group;

       end

        for gg=1:length(groups)

            % For each surface
            % Current surface
            cd = groups(gg);
            for ggg=1:length(cd)

                % Site list
                si = cd(ggg);
                si = si{1};
                slist = {};

                for siss=1:length(si)

                    % Current site
                    sisss = si(siss, 2);
                    site_name = si(siss, 1);
                    sisss = sisss{1};

                    fnames = fieldnames(sisss);

                    wolman_distances = {};

                    if length(fnames) > 0

                        s_weights = {};
                        s_wolmans = {};
                        
                        % For each site group
                        for f=1:length(fnames)
                            
                            % Current site group
                            % E.g. when there are two fines for calibration
                            
                            fn = fnames(f);
                            csg = sisss.(fn{1});
                            
                            sg_wolmans = [];
                            sg_weights = [];
                            sg_distances = [];
                            
                            for p=1:length(csg)
                                gsd = csg(p);
                                sg_distances = [sg_distances,gsd.distance];
                                if strcmp(gsd.cover, 'full') > 0
                                   cover = 100; 
                                else
                                   cover = str2num(gsd.cover);
                                end
                                sg_weights = [sg_weights,floor(cover)];
                                sg_wolmans = [sg_wolmans;gsd.wolman];
                            end
                            
                            s_weights{f} = mean(sg_weights);
                            s_wolmans{f} = sg_wolmans;
                            s_distances{f} = mean(sg_distances);
                        end
                        
                        s_coords = gsd.coords;

                        wolman_matrix = [];
                        for www=1:length(s_weights)
                            wolman_m = nan(500,s_weights{www});
                            wolman_m(1:length(s_wolmans{www}),1:s_weights{www}) = repmat(s_wolmans{www}, 1, s_weights{www});
                            wolman_matrix = [wolman_matrix, wolman_m];
                        end

                        wolman_column = reshape(wolman_matrix,numel(wolman_matrix),1);
                        
                        if length(wolman_column) < 150000
                            dif = 150000 - length(wolman_column);
                            pad = nan(dif,1);
                            wolman_column = [wolman_column;pad];
                        end

                        slist = [slist; {s_distances{1},wolman_column, s_coords, site_name{1}}];
                    end
                end
                site_wolmans.(fan_surface_names{gg}) = slist;
            end
        end

        sw_fn = fieldnames(site_wolmans);
        
        surface_sorted = struct();
        
        for o=1:length(fan_surface_names)
           % Sort by distance
           sw_n = sw_fn(o);
           sw = site_wolmans.(sw_n{1});
           d_sorted = sortrows(sw,1);
                      
           gs_col_all = [];
           dist_col_all = [];
           site_name_col_all = {};
           
           for d=1:length(d_sorted)-1
               gs = d_sorted(d,2);
               dist = d_sorted(d,1);
               wm = rmmissing(gs{1});
               dist_col = repmat(dist{1},length(wm),1);
               site_name_col = repmat(d_sorted{d,4},length(wm),1);
               site_name_col_all = [site_name_col_all; cellstr(site_name_col)];
               gs_col_all = [gs_col_all; wm];
               dist_col_all = [dist_col_all; dist_col];
           end
           
           % First column is distance, second is gs
           surface_sorted.(sw_n{1}) = d_sorted;
           T = table(site_name_col_all,dist_col_all,gs_col_all, 'VariableNames', {'Site', 'dist_m', 'd_mm'});
           writetable(T, ['full_surface_GSD_no_sites', filesep, fan_name, '_', sw_n{1}, '.csv']);
   
        end
        distance_sorted.(fan_name) = surface_sorted;
    end
end


