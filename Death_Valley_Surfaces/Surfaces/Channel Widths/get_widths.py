#!/usr/bin/env python3

import sys, math
import fiona
import ogr
import osr
from shapely.geometry import MultiLineString, LineString, Point, shape
from shapely import wkt
import rasterio
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
import csv

def raster_values(x_list,y_list,band,affine):

    row, col = ~affine * (np.array(x_list), np.array(y_list)) # x, y to raster coords
    dims = band.shape
    col = col.astype(int)
    row = row.astype(int)

    neg_cols = col < 0

    coord_distances = []

    raster_vals = band[col,row]

    coord_distances.append(0)
    for n, x in enumerate(x_list):
        if n > 0:
            dist = math.hypot(x_list[n-1] - x, y_list[n-1] - y_list[n])
            coord_distances.append(dist)

    raster_vals[neg_cols] = 0

    return raster_vals, coord_distances

def get_transect_data(x_orig, y_orig, x_end, y_end, band, affine):

    distance = 5 # Space measurements
    current_dist = distance

    x_list = []
    y_list = []

    max_len = math.hypot(x_orig - x_end, y_orig - y_end) # distance

    transect_line = shape(LineString([(x_orig, y_orig), (x_end, y_end)]))

    while current_dist < max_len:
        ## use interpolate and increase the current distance
        interp_point = transect_line.interpolate(current_dist)
        d = list(interp_point.xy)
        x_list.append(d[0][0])
        y_list.append(d[1][0])
        current_dist += distance

    l1_data, l1_dist = raster_values(x_list,y_list,band,affine)

    return l1_data, l1_dist

def getAngle(pt1, pt2):
    x_diff = pt2.x - pt1.x
    y_diff = pt2.y - pt1.y
    return math.degrees(math.atan2(y_diff, x_diff))

## start and end points of chainage tick
## get the first end point of a tick
def getPoint1(pt, bearing, dist):
    angle = bearing + 90
    bearing = math.radians(angle)
    x = pt.x + dist * math.cos(bearing)
    y = pt.y + dist * math.sin(bearing)
    return Point(x, y)

## get the second end point of a tick
def getPoint2(pt, bearing, dist):
    bearing = math.radians(bearing)
    x = pt.x + dist * math.cos(bearing)
    y = pt.y + dist * math.sin(bearing)
    return Point(x, y)

fans = ['hp', 'gc', 'so1', 'so2', 'ub']

# Load DEM
tick_lengths = [1000, 1000, 400, 400, 150]
xlim_lengths = [400, 250, 150, 200, 100]

for nf, fn in enumerate(fans):
    filename_active = './' + fn + '_channel_raster.tif' # MUST BE UTM
    filename_full= './' + fn + '_channel_raster_full.tif' # MUST BE UTM

    with rasterio.open(filename_active) as source:
        active_channel = source.read(1) # Read raster band 1 as a numpy array
        affine_active = source.affine

    with rasterio.open(filename_full) as source:
        full_channel = source.read(1) # Read raster band 1 as a numpy array
        affine_full = source.affine

    parallels = fiona.open(fn + '_parallel.shp')

    parallel = parallels.next()

    distance = 5
    ## the length of each tick
    tick_length = tick_lengths[nf]

    ## list to hold all the point coords
    list_points = []
    ## set the current distance to place the point
    current_dist = distance
    ## get the geometry of the line as wkt

    shapely_line = shape(parallel['geometry'])

    ## get the total length of the line
    line_length = shapely_line.length

    ## append the starting coordinate to the list
    first_coord = Point(parallel['geometry']['coordinates'][0])
    last_coord = Point(parallel['geometry']['coordinates'][-1])

    list_points.append(first_coord)


    while current_dist < line_length:
        ## use interpolate and increase the current distance
        list_points.append(shapely_line.interpolate(current_dist))
        current_dist += distance
    ## append end coordinate to the list
    list_points.append(last_coord)
    driver = ogr.GetDriverByName('Esri Shapefile')

    srs = osr.SpatialReference()
    srs.ImportFromEPSG(32611) # For DV

    driver = ogr.GetDriverByName('Esri Shapefile')
    ds = driver.CreateDataSource(fn+'_transect_chains.shp')

    out_ln_lyr = ds.CreateLayer('', srs, ogr.wkbMultiLineString)

    ds_p = driver.CreateDataSource(fn+'_transect_points.shp')
    out_ln_lyr_points = ds_p.CreateLayer('', srs, ogr.wkbPoint)

    active_widths = []
    full_widths = []

    with open(fn+'_stats.csv', 'w+') as File:
        writer = csv.writer(File, delimiter=',')

        for num, pt in enumerate(list_points, 1):
            print('Processing: '+str(num)+'/'+str(len(list_points)))
            ## start chainage 0
            if num == 1:
                current_point = list_points[num]
                angle_1 = getAngle(pt, list_points[num])
                line_end_1 = getPoint1(pt, angle_1, tick_length/2)
                angle_2 = getAngle(line_end_1, pt)
                line_end_2 = getPoint2(line_end_1, angle_2, tick_length)

            ## everything in between
            if num < len(list_points) - 1:
                current_point = list_points[num]
                angle_1 = getAngle(pt, current_point)
                line_end_1 = getPoint1(current_point, angle_1, tick_length/2)
                angle_2 = getAngle(line_end_1, current_point)
                line_end_2 = getPoint2(line_end_1, angle_2, tick_length)

            ## end chainage
            if num == len(list_points):
                current_point = list_points[num-2]
                angle_1 = getAngle(list_points[num - 2], pt) # Angle from line to point
                line_end_1 = getPoint1(pt, angle_1, tick_length/2)
                angle_2 = getAngle(line_end_1, pt) # Angle from
                line_end_2 = getPoint2(line_end_1, angle_2, tick_length)


            active_data, active_dists = get_transect_data(current_point.x, current_point.y, line_end_1.x, line_end_1.y, active_channel, affine_active) # Moving away from centre point
            full_data, full_dists = get_transect_data(current_point.x, current_point.y, line_end_1.x, line_end_1.y, full_channel, affine_full) # Moving away from centre point



            tick = LineString([(current_point.x, current_point.y), (line_end_1.x, line_end_1.y)])
            feat_dfn_ln = out_ln_lyr.GetLayerDefn()
            feat_ln = ogr.Feature(feat_dfn_ln)
            feat_ln.SetGeometry(ogr.CreateGeometryFromWkt(tick.wkt))
            out_ln_lyr.CreateFeature(feat_ln)
            feat_ln.Destroy()

            active_widths.append(np.sum(active_data)*5)
            full_widths.append(np.sum(full_data)*5)

            # point = Point(line_end_1.x, line_end_1.y)
            #
            # feat_dfn_point = out_ln_lyr_points.GetLayerDefn()
            # feat_point = ogr.Feature(feat_dfn_point)
            # feat_point.SetGeometry(ogr.CreateGeometryFromWkt(point.wkt))
            # out_ln_lyr_points.CreateFeature(feat_point)
            # feat_point.Destroy()

        cmap = plt.get_cmap('jet')
        active_c = cmap(0.5)
        full_c =cmap(0.8)
        width_size = 5.4 # inches...
        height_size = 3

        f = plt.figure(figsize =(width_size, height_size))

        handles = [Rectangle((0,0),1,1,color=c,ec="k") for c in [active_c,full_c]]

        n_1, bins, patches_active = plt.hist(active_widths, 10,  normed=True)
        n_2, bins, patches_full = plt.hist(full_widths, 10,  normed=True)

        for p in patches_active:
            p.set_facecolor(active_c)

        for p in patches_full:
            p.set_facecolor(full_c)

        a_mean = str(np.round(np.mean(active_widths)))
        a_std = str(np.round(np.std(active_widths)))
        f_mean = str(np.round(np.mean(full_widths)))
        f_std = str(np.round(np.std(full_widths)))

        writer.writerow(['active', a_mean, a_std])
        writer.writerow(['full', f_mean, f_std])

        n = n_1 + n_2

        plt.xlabel('Width (m)')
        plt.ylabel('Probability')
        plt.title(fn)
        plt.axis([0, xlim_lengths[nf], 0, max(n)+(max(n)/4)])
        #plt.text(10, max(n)-(max(n)/6), r'Q4 channel: $\bar x$='+a_mean+',$\ \sigma$='+a_std)
        #plt.text(10, max(n)-(max(n)/4), r'Q3 channel: $\bar x$='+f_mean+',$\ \sigma$='+f_std)
        plt.grid(True)

        labels= ["Q4 widths", "Q3 widths"]
        plt.legend(handles, labels)

        plt.show()
        f.savefig(fn + '_parallel.pdf', bbox_inches='tight')

        File.close()
